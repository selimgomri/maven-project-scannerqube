package org.sonar;

import java.util.*;

public class App {
    public App() {
        // System.out.println("C'est le constructeur ....");
    }

    public void methodeA() {
        // String chaine="";
        List<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        Object obj = getInfo();
        if (obj != null) {
            System.out.println(obj.toString());
        }
    }

    public Object getInfo() {
        return null;
    }

    public static void main(String[] args) {
        // System.out.println( "Hello World!" );
        App app = new App();
        app.methodeA();
    }
}